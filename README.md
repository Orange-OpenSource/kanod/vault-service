# Vault service

An image for deploying Vault. For development only (file back-end, no HA).

The image expects a persistent volume in `/data`. It is deployed with `kanod-vm`
or as a bootable iso with `kanod-prepare`

# Structure of the Vault service
Vault-service is not intended to be deployed in production but provides an
example of the structure of a Vault deployment to be used with Kanod so that
machine manifest can benefit from the presence of the Vault service to protect
the credentials necessary.

The base idea is that each machine is associated to an application role
(AppRole), the policy of the AppRole should give access to the secrets
necessary. The default policy is to use a folder for each role under each
engine understood by kanod-configure. So far we have developped entries for

* Key/Value stores version 1 and 2 deployed under respectively the `secret`
  and `kv` path
* PKI under the `pki` path

There is also a core `vault` application role whose role is to create tokens
for the other roles. It is used by `kanod-boot` utilities during the launch
of the servers.

The vault service deploys also a hidden `pkiroot` mount that host the `pki` for
a Certificate Authority that is the root of the intermediate authority facing
the Kanod services. In a real deployment this certificate authority would not
be deployed on the Vault server but would still be used as root of the
certificate for the Vault service and for the intermediate authority.

# Configuration of the vault service
All the specific configuration is under the `vault-service` key in the
configuration file:
* `ip` the IP of the service 
* `ca` a bundle containing the private key and the certificate of the
  main certificate authority. It will also be used as the root of the 
  certificate used by the vault server
* `admin` description of the administrator (user of the service):
   * `password` the password to log-in to Vault (userpass with login `admin`)
   * `policy`policy for the admin user in HCL format
* `approles` a list of application roles:
   * `name` the name of the role
   * `policy` an optional policy that overrides the default policy. The default
     policy let the role read in its folder for the secret and kv mounts and
     create key/certificate pair.
* `kv1` a list of secret definitions for the key/value secret engine:
   * `path` the path where the secret is stored under the `secret` node
   * `value` a dictionary made of key/value pairs
* `kv2` a list of secret definitions for the key/value secret engine version 2:
   * `path` the path where the secret is stored under the `kv` node
   * `value` a dictionary made of key/value pairs
