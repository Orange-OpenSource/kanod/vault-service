#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


basedir=$(dirname "${BASH_SOURCE[0]}")

url=${REPO_URL}
if [ -z "${url}" ]; then
  echo "No Nexus repository defined (fail)"
  exit 1
fi

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')
version="$IMAGE_VERSION"
artifactId="vault-service"

if [ -n "$version" ]; then
  if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.qcow2"; then
    echo "Building vault-service image"
    "${basedir}/make-image.sh"
    if [ -f vault.qcow2 ]; then
      # shellcheck disable=SC2086
      mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file  ${MAVEN_OPTS} \
        -DgroupId="${groupId}" -DartifactId="${artifactId}" \
        -Dversion="${version}" -Dtype=qcow2 -Dfile=vault.qcow2 \
        -DrepositoryId=kanod -Durl="${url}" \
        -Dfiles="${basedir}/vault-schema.yaml" -Dtypes=yaml  -Dclassifiers=schema
    else
        echo "Build for vault-services failed"
    fi
  fi
fi

