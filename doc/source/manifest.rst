Using Vault in Manifests
========================

To use Vault, the manifest of a VM must contain a vault field. The specification
of its content is given in :ref:`common_schema.yaml#/definitions/vault_config`

The common part of ``kanod-configure`` is the core component of the solution.
With the role-id that was in the initial manifest and the secret-id that was
added by ``kanod-vm``, it generates a token. This token is used to transform
all the values of the manifest that begin with the keyword: ``@vault:``. The
shape of those entries is:

    @vault:method:arg1: ... :argn

The method identifies how the real value is extracted from Vault:

``kv1:<path>:<key>``
    a secret stored in the key value store version 1 under 
    ``/secret/<role>/<path>`` with key ``<key>``

``kv2:<path>:<key>``
    a secret stored in the key value store version 2 under 
    ``/kv/<role>/<path>`` with key ``<key>``

``pki-key:<cert-name>``
    the private key associated with ``<cert-name>``. X509
    certificates used must be declared in the ``vault.certificates`` entry 
    wich is a list (see the content below).

``pki-cert:<cert-name>`` 
    the certificate itself

``pki-ca-chain:<cert-name>``
    the chain to the CA without the certificate

``pki-chain:<cert-name>``
    the chain to the CA including the certificate itself.

``ca``
    the Vault CA certificate. Usually enough for client machine so that
    they can validate all the machines using certificates managed by the
    vault certification authority.
