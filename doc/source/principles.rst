Vault Integration in Kanod - Principles
=======================================

Requirements for gitops and confidential assets
-----------------------------------------------
Some of the resources describing the infrastructure cannot be stored in
plain text in the git server.

Those assets must be encrypted. The encryption key must be only accessible to
authorized programs that run under the control of an authorized operator.

Those assets should be manageable: keys, certificates should be rotated on a
periodic basis. They must be revokable when needed.

We have decided to use a single, proven solution for the management of those
assets: Vault. This document describes how assets coming from Vault are injected
in the declarative model of the infrastructure.

Different kind of resources
---------------------------

There are two kinds of resources managed by OpenKaas:

* Kubernetes resourses inside the management cluster
* OS customizations (parts of cloud-init manifest) handled by ``kanod-vm``
  (during the boot of the management cluster and companion servers) or
  the updater (during the creation of clusters).

Most of the credentials are associated to the second kind of resources.

Actors and Policies in Vault
----------------------------
We will use Vault approles. An approle represent an application in a system.
A Vault token for an approle is obtained by combining a roleid and a secretid.
Both resources can be considered as secrets protected with different policies.
A policy can be associated to the approle that describes what it can perform.

Ideally, both are maintained by separate actors and only combined in the
application but we will also use it as a way to release rights: the role id
is fixed and known and the owner of the secret id just generates a new token
with a policy different (and less powerful) than his own.

The main actor is the infra admin. It is identified as a human. This actor
should have the power to generate tokens for the ``boot`` approle. This approle
is the one used by ``kanod-vm``. It can generate secret id for most of the
other app-roles.

Different VMs have different approle associated. The role-id is frozen in the
template describing the VM. The secret-id on the other hand is generated by
kanod-vm.

A special case is associated to the LCM. The LCM can create the updater secret
id during the boot of the VM. This secret id is injected within the stack.
Updaters create machine deployments and control-plane machines that contain
both the secret-id and the role-id associated to their application role.

.. figure:: approles.png
    :width: 80%

    Application Roles

Injection in templates by kanod-vm
----------------------------------

``kanod-vm`` creates a secret-id just before the cloud-init is created and the
VM is launched. The principle is the same for ``okaas-prepare`` but it is
important that the ISO image generated is consumed quickly as a virtual media.


Injection in templates in the updater
-------------------------------------

The updater is quite similar to the ``kanod-vm`` case. It also generates ``secret-id``. This will be replaced at some point by a reference for the ArgoCD application handl
ing the snapshot. In that case it will be directly
the clusterdef operator that will manage the authentification credentials
for ArgoCD when the application is created.

ArgoCD integration with Vault
-----------------------------
The previous mechanisms apply to OS image and cannot protect the credentials
of the baremetal hosts BMCs or the git credentials of the projects. For this
kanod uses
`IBM Vault plugin for ArgoCD <https://github.com/IBM/argocd-vault-plugin>`_.

The principle is that protected resources should be specified in a special
syntax identifying them as pointers to Vault: ``<path: ... >``. There is a
single role used by argocd: the ``argocd`` approle.

Please read the
`official documentation <https://ibm.github.io/argocd-vault-plugin/v1.4.0/howitworks/>`_
for more details. Note the following points:

* the paths used my point to a key/value secret version 2 visible by
  the argocd approle. In Vault configuration for Kanod in a bottle, it should
  follow the following structure: ``kv/data/argocd/subpath#key``.
* The plugin recognizes base64 encoded strings. So it is possible to specify
  a path in a secret generator
* It is not necessary to encode in base64 the secret in the Vault server. One
  can use the base64 transformer in the specification of the path in the
  Kubernetes resource.

Here is an example of kustomization file with a secret in Vault:

.. code:: yaml

  secretGenerator:
  - name: secret-name
    namespace: secret-namespace
    literals:
    - password=<path:kv/data/argocd/credential_path#password
    - username=<path:kv/data/argocd/credential_path#user
