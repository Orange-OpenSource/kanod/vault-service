#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.


import os
from pathlib import Path
import requests
import shutil
import stat
import subprocess
import sys
import time

from kanod_configure import common

BASE_URL = 'http://localhost:8200/v1'

year = 3600 * 24 * 365


def initialize_auths(token):
    print('Setting up auth methods')
    for kind in ['userpass', 'approle']:
        req = requests.post(
            f'{BASE_URL}/sys/auth/{kind}',
            headers={'X-Vault-Token': token},
            json={'type': kind})
        if req.status_code != 204:
            print(
                f'failed to initialize auth method {kind} '
                f'({req.status_code})')


def initialize_engines(token):
    print('Setting up engines')
    req = requests.post(
        f'{BASE_URL}/sys/mounts/secret',
        headers={'X-Vault-Token': token},
        json={'type': 'kv', 'options': {'version': '1'}})
    if req.status_code != 204:
        print(f'failed to initialize secret ({req.status_code})')
    req = requests.post(
        f'{BASE_URL}/sys/mounts/kv',
        headers={'X-Vault-Token': token},
        json={'type': 'kv', 'options': {'version': '2'}})
    if req.status_code != 204:
        print(f'failed to initialize kv ({req.status_code})')
    for endpoint in ['pki', 'pkiroot']:
        req = requests.post(
            f'{BASE_URL}/sys/mounts/{endpoint}',
            headers={'X-Vault-Token': token},
            json={'type': 'pki', 'max_lease_ttl': year})
        if req.status_code != 204:
            print(f'failed to initialize pki ({req.status_code})')


def initialize_ca(conf, token):
    print('initializing CA')
    issuer = conf.get('ca', None)
    if issuer is None:
        print('No CA to configure')
        return

    req = requests.post(
        f'{BASE_URL}/pkiroot/config/ca',
        headers={'X-Vault-Token': token},
        json={'pem_bundle': issuer}
    )
    if req.status_code != 204:
        print(
            f'failed to set root ca ({req.status_code}):'
            f' {req.content.decode("utf-8")}')
    req = requests.post(
        f'{BASE_URL}/pki/intermediate/generate/internal',
        headers={'X-Vault-Token': token},
        json={
            'common_name': 'kanod-intermediate',
            'ttl': year, 'max_ttl': year}
    )
    if req.status_code != 200:
        print(
            'failed to generate intermediate'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return
    csr = req.json().get('data', {}).get('csr', None)
    if csr is None:
        print('pki: did not find the intermediate csr')
        return
    req = requests.post(
        f'{BASE_URL}/pkiroot/root/sign-intermediate',
        headers={'X-Vault-Token': token},
        json={
            'common_name': 'kanod-intermediate', 'csr': csr,
            'ttl':  year, 'max_ttl': year}
    )
    if req.status_code != 200:
        print(
            'failed to sign intermediate csr'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return
    data = req.json().get('data', {})
    cert = data.get('certificate', '')
    issuer = data.get('issuing_ca', '')
    full_chain = cert + '\n' + issuer
    req = requests.post(
        f'{BASE_URL}/pki/intermediate/set-signed',
        headers={'X-Vault-Token': token},
        json={'certificate': full_chain}
    )
    if req.status_code != 204:
        print(
            'Failed to set intermediate cert'
            f' ({req.status_code}): {req.content.decode("utf-8")}')
        return


def initialize_roles(conf, token, use_tpm=False):
    print('Setting up application roles')
    roles = conf.get('approles', [])
    if use_tpm:
        roles.append({
            'name': 'tpm_auth',
            'policy': '''
              path "kv/data/machines/*" {
                  capabilities = ["read"]
              }
              path "auth/approle/role/+/secret-id" {
                  capabilities = ["create", "update"]
              }
            '''
        })
    roles.append({
        'name': 'vault',
        'policy': '''
           path "auth/approle/role/*" {
               capabilities = ["read","update","create"]
           }
        '''
    })
    for role in roles:
        name = role.get('name', None)
        id = role.get('id', None)
        policy = role.get('policy', None)
        extends = role.get('extends', False)
        if name is None:
            print('need name for each approle')
            continue
        default_policy = f'''
            path "secret/{name}/*" {{ capabilities = ["read"] }}
            path "kv/data/{name}/*" {{ capabilities = ["read"] }}
            path "pki/issue/{name}" {{
                capabilities = ["read","update","create"]
            }}
            path "pki/ca/pem" {{ capabilities = ["read"] }}
            '''
        if policy is None:
            policy = default_policy
        else:
            if extends:
                policy = default_policy + policy
        req = requests.put(
            f'{BASE_URL}/sys/policies/acl/{name}',
            headers={'X-Vault-Token': token},
            json={'policy': policy}
        )
        if req.status_code != 204:
            print(
                f'failed to create policy {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        req = requests.put(
            f'{BASE_URL}/auth/approle/role/{name}',
            headers={'X-Vault-Token': token},
            json={
                'token_policies': name,
                'token_ttl': '1h',
                'token_max_ttl': '1h'
            })
        if req.status_code != 204:
            print(
                f'failed to bind approle {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        req = requests.post(
            f'{BASE_URL}/pki/roles/{name}',
            headers={'X-Vault-Token': token},
            json={'allow_any_name': True, 'ou': 'kanod'}
        )
        if req.status_code != 204:
            print(
                f'failed to create pki role for {name} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
        if id is not None:
            req = requests.post(
                f'{BASE_URL}/auth/approle/role/{name}/role-id',
                headers={'X-Vault-Token': token},
                json={'role_id': id}
            )
            if req.status_code != 204:
                print(
                    f'failed to bind role-id {id} to role {name} '
                    f'({req.status_code}): {req.content.decode("utf-8")}')


def initialize_k8s(conf, token):
    for k8s_spec in conf.get('kubernetes', []):
        name = k8s_spec.get("name", None)
        if name is None:
            print('Kubernetes entry without name. Skipping it')
            continue
        print(f'Setting up kubernetes cluster {name}')
        req = requests.post(
            f'{BASE_URL}/sys/auth/kubernetes/{name}',
            headers={'X-Vault-Token': token},
            json={'type': 'kubernetes'})
        if req.status_code != 204:
            print(
                f'failed to initialize kubernetes method for cluster {name} '
                f'({req.status_code})')
            continue
        roles = k8s_spec.get("roles", [])
        for role in roles:
            role_name = role.get('name', None)
            serviceaccount = role.get('account', 'default')
            namespace = role.get('namespace', 'default')
            policy = role.get('policy', None)
            extends = role.get('extends', False)
            default_policy = f'''
              path "secret/{name}/{role_name}/*" {{ capabilities = ["read"] }}
              path "kv/data/{name}/{role_name}/*" {{ capabilities = ["read"] }}
              path "pki/ca/pem" {{ capabilities = ["read"] }}
              '''
            if policy is None:
                policy = default_policy
            else:
                if extends:
                    policy = default_policy + policy
            req = requests.put(
                f'{BASE_URL}/sys/policies/acl/{name}-{role_name}',
                headers={'X-Vault-Token': token},
                json={'policy': policy}
            )
            if req.status_code != 204:
                print(
                    f'failed to create policy for k8s {name}-{role_name} '
                    f'({req.status_code}): {req.content.decode("utf-8")}')
            req = requests.post(
                f'{BASE_URL}/auth/kubernetes/{name}/role/{role_name}',
                headers={'X-Vault-Token': token},
                json={
                    'bound_service_account_names': [serviceaccount],
                    'bound_service_account_namespaces': [namespace],
                    'token_policies': [f'{name}-{role_name}']
                }
            )


def initialize_secrets(conf, token):
    for spec in conf.get('kv1', []):
        path = spec.get('path', None)
        value = spec.get('value', None)
        if path is None or value is None:
            continue
        req = requests.post(
            f'{BASE_URL}/secret/{path}',
            headers={'X-Vault-Token': token},
            json=value)
        if req.status_code != 204:
            print(
                f'failed to set secret at {path} '
                f'({req.status_code}): {req.content.decode("utf-8")}')
    for spec in conf.get('kv2', []):
        path = spec.get('path', None)
        value = spec.get('value', None)
        if path is None or value is None:
            continue
        req = requests.post(
            f'{BASE_URL}/kv/data/{path}',
            headers={'X-Vault-Token': token},
            json={'data': value})
        if req.status_code != 200:
            print(
                f'failed to set secret at {path} '
                f'({req.status_code}): {req.content.decode("utf-8")}')


def initialize_tpm_auth(conf, token):
    print(f'Setting up TPM authenticator')
    req = requests.get(
        f'{BASE_URL}/auth/approle/role/tpm_auth/role-id',
        headers={'X-Vault-Token': token})
    if req.status_code != 200:
        print(f'cannot get role-id for tpm_auth')
        return
    role_id = req.json().get('data', {}).get('role_id')
    req = requests.post(
        f'{BASE_URL}/auth/approle/role/tpm_auth/secret-id',
        headers={'X-Vault-Token': token})
    secret_id = req.json().get('data', {}).get('secret_id')
    url = BASE_URL.replace('/v1', '')
    path = '/data/tpm-auth.config'
    common.render_template(
        'vault_tpm_auth.tmpl', path,
        {'role_id': role_id, 'secret_id': secret_id, 'url': url})
    os.chmod(path, 0o400)
    shutil.chown(path, user="tpmauth", group="tpmauth")


def initialize_admin(conf, token):
    print('Setting up admin user')
    admin_conf = conf.get('admin', {})
    password = admin_conf.get('password', None)
    policy = admin_conf.get('policy', None)
    if password is None or policy is None:
        print('Need password and policy for admin')
        return
    req = requests.put(
        f'{BASE_URL}/sys/policies/acl/adminpol',
        headers={'X-Vault-Token': token},
        json={'policy': policy}
    )
    if req.status_code != 204:
        print(
            'failed to create policy for admin '
            f'({req.status_code}): {req.content.decode("utf-8")}')
    req = requests.post(
        f'{BASE_URL}/auth/userpass/users/admin',
        headers={'X-Vault-Token': token},
        json={'password': password, 'policies': ['adminpol']}
    )
    if req.status_code != 204:
        print(
            'failed to create admin '
            f'({req.status_code}: {req.content.decode("utf-8")})')


def initialize_vault():
    print('Initializing vault\n')
    datapath = Path('/data/vault-data')
    key_path = Path('/data/vault_key')
    token = None
    if not key_path.is_file():
        print('* Bootstrap store')
        if not datapath.is_dir():
            datapath.mkdir(mode=stat.S_IRUSR | stat.S_IWUSR | stat.S_IXUSR)
        shutil.chown(datapath, user='vault', group='vault')
        req = requests.put(
            f'{BASE_URL}/sys/init',
            json={'secret_shares': 1, 'secret_threshold': 1})
        if req.status_code != 200:
            print('Cannot initialize Vault')
            return
        content = req.json()
        key = content['keys'][0]
        token = content['root_token']
        print('* Export keys')
        with open(key_path, 'w') as fd:
            fd.write(key)
            fd.write('\n')
        with open('/home/admin/token', 'w') as fd:
            fd.write(token)
            fd.write('\n')
    if key_path.is_file():
        print('* Unsealing')
        with open(key_path, 'r') as fd:
            key = fd.readline()
        req = requests.put(
            f'{BASE_URL}/sys/unseal',
            json={'key': key[:64]}
        )
        if req.status_code != 200 or req.json()['progress'] != 0:
            print('Cannot unseal the vault' + str(req.json()))
            return
        return token


def setup_proxy(conf, token):
    ip_address = conf.get('ip', None)
    if ip_address is None:
        print('Cannot set-up proxy without ip address')
        return
    json = {'common_name': 'vault', 'ip_sans': ip_address}
    req = requests.post(
        f'{BASE_URL}/pki/issue/vault',
        headers={'X-Vault-Token': token},
        json=json
    )
    if req.status_code != 200:
        print(
            f'Failed to generate certificate for vault'
            f'({req.status_code}): {req.content.decode("utf-8")}')
        return
    data = req.json().get('data', {})
    cert = data.get('certificate', None)
    key = data.get('private_key', None)
    ca_chain = data.get('ca_chain', None)

    if cert is None or key is None or ca_chain is None:
        print(f'Something wrong during generation of cert for vault')
        return
    folder = Path('/data/certs')
    os.mkdir(folder)
    with open(folder.joinpath('key.pem'), 'w') as fd:
        fd.write(key)
        fd.write('\n')
    with open(folder.joinpath("cert.pem"), 'w') as fd:
        fd.write(cert)
        fd.write('\n')
        for c in ca_chain:
            fd.write(c)
            fd.write('\n')


def setup_services(conf):
    if os.path.exists('/data/tpm-auth.config'):
        subprocess.run(
            ["systemctl", "enable", "--now", "tpm-auth"],
            stdout=sys.stdout, stderr=subprocess.STDOUT)
    subprocess.run(
        ['systemctl', 'enable', 'vault-unseal'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)


def launch_proxy(conf, use_tpm):
    ip_address = conf.get('ip', None)
    if ip_address is None:
        print('Cannot launch proxy without ip address')
        return
    common.render_template(
        'vault_proxy.tmpl',
        'etc/nginx/sites-available/proxy',
        {'ip_address': ip_address, 'use_tpm': use_tpm})
    os.symlink(
        '/etc/nginx/sites-available/proxy',
        '/etc/nginx/sites-enabled/proxy')
    proc = subprocess.run(
        ['systemctl', 'enable', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    proc = subprocess.run(
        ['systemctl', 'start', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)
    if proc.returncode != 0:
        print('Configuring proxy failed.')
    time.sleep(20)
    # TODO(cregut) Restarting does not make sense as everything is
    # sequential but it makes it work...
    proc = subprocess.run(
        ['systemctl', 'restart', 'nginx'],
        stdout=sys.stdout, stderr=subprocess.STDOUT)


def configure_vault_service(arg: common.RunnableParams):
    conf = arg.conf
    vault_conf = conf.get('vault_service', {})
    token = initialize_vault()
    use_tpm = os.path.exists('/usr/local/bin/tpm-auth')
    if token is not None:
        initialize_auths(token)
        initialize_engines(token)
        initialize_ca(vault_conf, token)
        initialize_roles(vault_conf, token, use_tpm)
        initialize_k8s(vault_conf, token)
        initialize_admin(vault_conf, token)
        setup_proxy(vault_conf, token)
        initialize_secrets(vault_conf, token)
        if use_tpm:
            initialize_tpm_auth(vault_conf, token)
    setup_services(vault_conf)
    launch_proxy(vault_conf, use_tpm)


common.register('Vault service', 200, configure_vault_service)
