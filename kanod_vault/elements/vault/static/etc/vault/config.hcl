storage "file" {
  path    = "/data/vault-data"
}

listener "tcp" {
  address = "localhost:8200"
  tls_disable = 1
}

listener "tcp" {
  address = "localhost:8201"
  tls_disable = 1
  proxy_protocol_behavior = "use_always"
}

ui = true
max_lease_ttl="8784h"
